package no.uib.inf101.terminal;

// UiB INF101 ShellLab - SimpleShell.java
//
// Dette er klassen vi skal forbedre i denne lab'en. Slik koden er
// allerede før du begynner på laben, støtter den tre kommandoer:
//
// - cd: Bytt til en annen mappe
// - ls: List opp filer i mappen
// - pwd: Vis sti til nåværende mappe
//
// Vi skal endre denne klassen slik at den
// - kan vises av Terminal.java
// - kan støtte ubegrenset mange kommandoer

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class SimpleShell implements CommandLineInterface {

  //////////////////////////////////////////////////////////////////////
  /// Instance variables ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////

  /** The prompt to show before each command */
  private final String prompt = "$ ";
  /** The context variable contains cwd and home directories etc. */
  private final Context context = new Context();
  /** A list of historic commands and their outputs */
  private final List<String> history = new ArrayList<>();
  /** The command currently being typed */
  private String currentCommand = "";

  //////////////////////////////////////////////////////////////////////
  /// Public instance methods                                     //////
  /// (methods expected to be used by someone outside this class) //////
  //////////////////////////////////////////////////////////////////////
  private final HashMap<String, Command> allCommands = new HashMap<>();

  /** Constructor for SimpleShell */
  public SimpleShell() {
    // Install core commands SimpleShell supports (pwd, ls, cd)
    this.installCommand(new CmdPwd());
    this.installCommand(new CmdCd());
    this.installCommand(new CmdLs());


  }

  @Override
  public void keyPressed(char key) {
    System.out.println("Keycode pressed: '" + ((int) key) + "'"); // sjekker hvilken verdi som kommer når du trykker på backspace
    if (key == '\n') {
      this.processCurrentCommandLine();
    } else if (key >= ' ' && key <= '~') {
      this.currentCommand += key;
    } else if (key == 8 && (currentCommand.length() > 0)) { // 8 er backspace og lengden av strengen må være større enn 0 for at man skal kunne bruke backspace funksjonen
      this.currentCommand = this.currentCommand.substring(0, this.currentCommand.length() - 1);
    } else {
      // Some special key was pressed (e.g. shift, ctrl), we ignore it
    }
  }

  @Override
  public String getScreenContent() {
    String s = "";
    for (String line : this.history) {
      s += line;
    }
    s += this.prompt;
    s += this.currentCommand;
    return s;
  }

  //////////////////////////////////////////////////////////////////////
  /// Private methods                                ///////////////////
  /// (helper methods used internally in this class) ///////////////////
  //////////////////////////////////////////////////////////////////////
  // method to install a command
  public void installCommand(Command command) { 
    this.allCommands.put(command.getName(), command);
    // et kall til setContext
    command.setContext(this.context);
    // et kall til setCommandContext
    command.setCommandContext(this.allCommands);
  }

  /**
   * Process the current command line. This entails splitting it into
   * a command name and arguments; executing the command; and adding
   * the result to the history.
   */
  private void processCurrentCommandLine() {
    String result = "";
    if (this.currentCommand.length() > 0) {
      String[] args = this.currentCommand.split(" ");
      String commandName = args[0];
      String[] commandArgs = new String[args.length - 1];
      System.arraycopy(args, 1, commandArgs, 0, commandArgs.length);
      result = this.executeCommand(commandName, commandArgs);
      if (result.length() > 0 && result.charAt(result.length() - 1) != '\n') {
        result += '\n';
      }
    }
    this.history.add(this.prompt + this.currentCommand + "\n" + result);
    this.currentCommand = "";
  }

  /**
   * Execute a command with the given name and arguments. The command
   * name could be "ls", "cd", "pwd", etc., and the arguments are the
   * arguments to the command. For example for the command "cd foo", the
   * command name is "cd" and the argument comes in the array ["foo"].
   *
   * @param commandName  The name of the command to execute
   * @param args  The arguments to the command
   * @return  The output of the command
   */
  private String executeCommand(String commandName, String[] args) {
    // Det er i denne metoden at vi bestemmer at ulike ting skal skje basert på hvilken kommando som blir gitt
      Command command = this.allCommands.get(commandName);
      if (command != null) {
        return command.run(args);
      } else {
        return "Command not found: \"" + commandName + "\"";
      }
  }
}
