package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {
    String run(String[] args); // metodesignatur
    String getName(); // metode for å gi oss navnet til kommandoen

    // en metode-signatur for å sette kontekst.
    /*
     * Denne linjen oppretter en metode i grensesnittet med en standard (default) implementasjon 
     * (som altså her er å ikke gjøre noen ting som helst dersom metoden blir kalt). Dette innebærer
     *  imidlertid at vi nå kan kalle på setContext dersom vi har å gjøre med et Command-objekt.
     */
    default void setContext(Context context) { /* do nothing */ };

    // returnerer instruksjoner for hvordan kommandoen brukes.
    String getManual();

    // en default-metode ed en standard-implementasjon som ikke gjør noen ting.
    default void setCommandContext(Map<String, Command> map) {};


}
