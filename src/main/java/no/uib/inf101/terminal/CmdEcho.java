package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String echoString = "";
        for (String element : args) {
            echoString += element;
            echoString += " ";
        }
        return echoString;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
