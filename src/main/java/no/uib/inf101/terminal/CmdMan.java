package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command {
    // instansvariabel
    Map<String, Command> map;

    @Override
    public String run(String[] args) {
        // hent ut riktig Command-objekt fra nevnte instansvariabel og 
        // returner resultatet av getManual() kalt på dette objektet.
        // args[0] er det første som skrives i terminalen
        Command newCmdMan = map.get(args[0]);
        // lager en ny variabel av typen Command som peker på objektet der man henter argument 0 fra orrdboken map
        return newCmdMan.getManual(); // returnerer manualen til den kommandoen man taster inn etter kodeordet 
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return "man kommandoen gir oss manualen til kommandoen";
    }

    // må ikke ha override på default metoder
    public void setCommandContext(Map<String, Command> map) {
        this.map = map; // initierer instansvariabelen
    } 
}
