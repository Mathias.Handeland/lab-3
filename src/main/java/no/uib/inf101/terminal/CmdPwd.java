package no.uib.inf101.terminal;

public class CmdPwd implements Command {
    // instansvariabler
    Context context;
    
    @Override
    public String run(String[] args) {
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        return "pwd"; // stringen til navnet som skrives i terminalen
    }

    @Override
    public void setContext(Context context) { 
        this.context = context;
    }

    @Override
    public String getManual() {
        return "'pwd' (print working directory) skriver ut\n hvilken mappe i filsystemet som er cwd\n (current working directory),\n altså den mappen man «er» i.";
    } 
}
